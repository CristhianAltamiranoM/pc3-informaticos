                           Vision Document

 Rev History

   Date     Version        Description               Author
  5/3/2019    1.0    Documentacion del proyecto     Malena Chinchay Arancibia
 _________________________________________________________________________________

1.INTRODUCCION

 1.1 Proposito: 
       Identificar actores de negocio, casos de uso de negocio,stakeholders,restricciones impuestas al sistema
       y definir los limites del sistema


 1.2 Definiciones

       Caso de uso: es una manera de capturar requerimientos , permiten enfocarse en la necesidad del usuario.
       Actores de negocio: Todas las personas que participan en la empresa.
       Stakeholders: persona externa que es afectada por los problemas de la empresa.	



2. OPORTUNIDAD DE NEGOCIO
 
 2.1 Oportunidad de negocio
       La empresa xyz desea continuar con su licencia de funcionamiento, para ello debe implementar un sistema gestion en base de datos que 
       garantice la normal operaciones de todas sus bases de datos repartidos en todos sus servidores.

 2.2 Formulacion de Problema

	PROBLEMA    falta de licenciamiento 
	AFECTA      toda la empresa
	IMPACTO     perdida de la empresa  
	SOLUCION    gestionar el sistema 
		

3.STAKEHOLDERS
 
 3.1 Stakeholders:

         +primarios:
             -administrador
             -Trabajador
	     -Gerente General

	 +secundarios:
             -Sunat
             -Tiempo
 3.2 Usuarios:
      
        -Usuario DBA
        -Usuario de Redes


          
             
		



      
   
        
